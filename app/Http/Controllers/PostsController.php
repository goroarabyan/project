<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Posts;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = auth()->user()->id;
        $posts = Posts::where('uid',$user_id)->paginate(10);
        return view('admin.allPosts',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.addPost');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user_id = auth()->user()->id;
        if ($request->hasFile('img')) {
            if ($request->file('img')->isValid()) {
                try {
                    $file = $request->file('img');
                    $path = 'images/uploads';
                    $iname = rand(11111, 99999) . '.' . $file->getClientOriginalExtension();
                    $name = $path . '/' . $iname;
                    $request->file('img')->move(public_path($path), $iname);

                } catch (Illuminate\Filesystem\FileNotFoundException $e) {

                }
            }
        }else {
            $name = '';
        }
        $store = Posts::store($request,$user_id,$name);
        return redirect()->route("posts.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.viewPost');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Posts::find($id);
        return view('admin.editPost',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->hasFile('img')) {
            if ($request->file('img')->isValid()) {
                try {
                    $file = $request->file('img');
                    $path = 'images/uploads';
                    $iname = rand(11111, 99999) . '.' . $file->getClientOriginalExtension();
                    $name = $path . '/' . $iname;
                    $request->file('img')->move(public_path($path), $iname);

                } catch (Illuminate\Filesystem\FileNotFoundException $e) {

                }
            }
        }else {
            $name = $request->img;
        }
        $store = Posts::_update($request,$id,$name);
        return redirect()->route("posts.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Posts::destroy($id);
        return redirect()->route('posts.index');
    }
}
