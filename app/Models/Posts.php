<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $table = "posts";

    protected $fillable = ['uid','title','img','small_titl1','small_tip1','small_titl2','small_tip2','middle_title','type','tags','active'];

    public static function store($request,$user_id ,$imagename){
        $post = new Posts();
        if($request->title){
            $post->title = $request->title;
        }
        if($user_id){
            $post->uid = $user_id;
        }
        if($imagename){
            $post->img = $imagename;
        }
        if($request->small_titl1){
            $post->small_titl1 = $request->small_titl1;
        }
        if($request->small_tip1){
            $post->small_tip1 = $request->small_tip1;
        }
        if($request->small_titl2){
            $post->small_titl2 = $request->small_titl2;
        }
        if($request->small_tip2){
            $post->small_tip2 = $request->small_tip2;
        }
        if($request->middle_title){
            $post->middle_title = $request->middle_title;
        }
        if($request->type){
            $post->type = $request->type;
        }
        if($request->tags){
            $tags = implode( ", ", $request->tags);
            $post->tags = $tags;
        }
        if($request->active){
            $post->active = $request->active;
        }
        if($request->active == "1"){
            $post->active = $request->active;
        }else{
            $post->active = "0";
        }
        $post->save();
        return $post;
    }
    public static function _update($request, $id, $imageName)
    {
        if(empty($imageName) || !$imageName){
            $imageName = $request->old_img;
        }
        $tags = implode( ", ", $request->tags);
        $post = Posts::find($id);
        $post->title = $request->title;
        $post->uid = $id;
        $post->img = $imageName;
        $post->small_titl1 = $request->small_titl1;
        $post->small_tip1 = $request->small_tip1;
        $post->small_titl2 = $request->small_titl2;
        $post->small_tip2 = $request->small_tip2;
        $post->middle_title = $request->middle_title;
        $post->type = $request->type;
        $post->tags = $tags;
        if($request->active == "1"){
            $post->active = $request->active;
        }else{
            $post->active = "0";
        }
        $post->save();
        return $post;
    }
}
