@extends('layouts.adminApp')
@section('style')
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <a class="btn btn-info text-light" href="{{ route('posts.create') }}">ADD NEW</a>
            </div>
        </div>
        <div class="row justify-content-center pt-4">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">All Posts ({{ $posts->total() }}) . <span>Showing {{ $posts->count() }} on this page.</span> </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Small Title 1</th>
                                    <th scope="col">Small Tip 1</th>
                                    <th scope="col">Small Title 2</th>
                                    <th scope="col">Small Tip 2</th>
                                    <th scope="col">Medium Title</th>
                                    <th scope="col">Type</th>
                                    <th scope="col">Tags</th>
                                    <th scope="col">Active</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            {{--{{ dd($posts) }}--}}
                            @foreach($posts as $key=>$post)
                                <tr>
                                    <th scope="row">{{$post->id}}</th>
                                    <td>{{$post->title}}</td>
                                    <td><img src="{{asset($post->img)}}" height="30"></td>
                                    <td>{{$post->small_titl1}}</td>
                                    <td>{{$post->small_tip1}}</td>
                                    <td>{{$post->small_titl2}}</td>
                                    <td>{{$post->small_tip2}}</td>
                                    <td>{{$post->middle_title}}</td>
                                    <td>{{$post->type}}</td>
                                    <td>{{$post->tags}}</td>
                                    <td>{{$post->active}}</td>
                                    <td class="actions">
                                        <a href="{{ route('posts.edit',$post->id) }}" class="edit_icon"><i class="fa fa-edit"></i></a>
                                        <form action="{{ route('posts.destroy',$post->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="trash_icon btn btn-light"><i class="fa fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $posts->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://code.jquery.com/jquery-3.4.1.slim.js" integrity="sha256-BTlTdQO9/fascB1drekrDVkaKd9PkwBymMlHOiG+qLI=" crossorigin="anonymous"></script>
    <script>
        $('.trash_icon').on('click',function(e){
            e.preventDefault();
            if(confirm("Are you sure?")){
                $(this).parent().submit()
            }
        })
    </script>
@endsection
