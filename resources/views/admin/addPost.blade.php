@extends('layouts.adminApp')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <a class="btn btn-info text-light" href="{{ route('posts.index') }}">ALL POSTS</a>
            </div>
        </div>
        <div class="row justify-content-center pt-4">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Add New Post</div>
                    <div class="card-body">
                        <form action="{{ route('posts.store') }}" enctype="multipart/form-data" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" name="title" class="form-control" id="title" required>
                            </div>
                            <div class="form-group">
                                <label for="img">Image</label>
                                <input type="file" name="img" class="form-control-file" id="img" required>
                            </div>
                            <div class="form-group">
                                <label for="small_titl1">Small Title 1</label>
                                <input type="text" name="small_titl1" max-length="50" class="form-control" id="small_titl1">
                            </div>
                            <div class="form-group">
                                <label for="small_tip1">Small Tip 1</label>
                                <input type="text" name="small_tip1" max-length="1000" class="form-control" id="small_tip1">
                            </div>
                            <div class="form-group">
                                <label for="small_titl2">Small Title 2</label>
                                <input type="text" name="small_titl2" max-length="50" class="form-control" id="small_titl2">
                            </div>
                            <div class="form-group">
                                <label for="small_tip2">Small Tip 2</label>
                                <input type="text" name="small_tip2" max-length="1000" class="form-control" id="small_tip2">
                            </div>
                            <div class="form-group">
                                <label for="middle_title">Middle Title</label>
                                <input type="text" name="middle_title" max-length="60" class="form-control" id="middle_title">
                            </div>
                            <div class="form-group">
                                <label for="type">Type</label>
                                <select class="form-control" name="type" id="type">
                                    <option value="A1">A1</option>
                                    <option value="A2">A2</option>
                                    <option value="B1">B1</option>
                                    <option value="B2">B2</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <div>
                                    <label>Tags</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="tags[]" id="t1" value="T1">
                                    <label class="form-check-label" for="t1">T1</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="tags[]" id="t2" value="T2">
                                    <label class="form-check-label" for="t2">T2</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="tags[]" id="t3" value="T3">
                                    <label class="form-check-label" for="t3">T3</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="tags[]" id="b1" value="B1">
                                    <label class="form-check-label" for="b1">B1</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" name="tags[]" id="b2" value="B2">
                                    <label class="form-check-label" for="b2">B2</label>
                                </div>
                                <div class="form-group">
                                    <div>
                                        <label for="active">Active</label>
                                    </div>
                                    <label class="switch">
                                        <input type="checkbox" name="active" id="active" value="1" checked>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="form-group col-md-4 m-auto">
                                    <input type="submit" name="" class="btn btn-success btn-block" value="Create" id="">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection