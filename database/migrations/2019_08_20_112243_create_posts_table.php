<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('uid');
            $table->string('title');
            $table->string('img')->nullable();
            $table->string('small_titl1',50)->nullable();
            $table->string('small_tip1',1000)->nullable();
            $table->string('small_titl2',50)->nullable();
            $table->string('small_tip2',1000)->nullable();
            $table->string('middle_title',60)->nullable();
            $table->enum('type',array("A1","A2","B1","B2"))->nullable();
            $table->string('tags')->nullable();
            $table->enum('active',array('0','1'))->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
